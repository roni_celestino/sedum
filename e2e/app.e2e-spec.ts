import { KaroluniformesPage } from './app.po';

describe('karoluniformes App', () => {
  let page: KaroluniformesPage;

  beforeEach(() => {
    page = new KaroluniformesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
