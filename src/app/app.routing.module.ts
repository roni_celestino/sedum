import { CanActivateChild } from '@angular/router';
import { LoginComponent } from './admin/login/login.component';
import { NgModule, ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

// MODULO
import { AdminModule } from './admin/admin.module';
import { SiteModule } from './site/site.module';

// GUARDA
import { LoginGuard } from './admin/guards/login.guard';
import { LoginCadastroComponent } from './admin/login/login-cadastro/login-cadastro.component';
import { LoginEntrarComponent } from './admin/login/login-entrar/login-entrar.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, children: [
    { path: '', redirectTo: 'entrar', pathMatch: 'full' },
    { path: 'entrar', component: LoginEntrarComponent},
    { path: 'cadastro', component: LoginCadastroComponent}
  ]
},


  { path: '', pathMatch: 'prefix', redirectTo: 'site' },
  { path: 'site', loadChildren: 'app/site/site.module#SiteModule' },
  {
    path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' //,
    // canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]

})

export class AppRoutingModule { }
