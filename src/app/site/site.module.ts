import { NavbarService } from './component/navbar/navbar.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SiteRoutingModule } from './site.routing.module';

import { SiteComponent } from './site.component';
import { HomeComponent } from './paginas/home/home.component';
import { FooterComponent } from './component/footer/footer.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { SlideComponent } from './component/slide/slide.component';
import { SlideService } from './component/slide/slide.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SiteRoutingModule
    ],

    declarations: [
        SiteComponent,
        HomeComponent,
        NavbarComponent,
        FooterComponent,
        SlideComponent
    ],
    exports: [    
    ],
    providers: [SlideService, NavbarService]

})


export class SiteModule { }