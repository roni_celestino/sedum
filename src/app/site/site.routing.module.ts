import { NgModule, ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

import { SiteComponent } from './site.component';
import { HomeComponent } from './paginas/home/home.component';
const siteRoutes: Routes = [
    {
        path: '', component: SiteComponent, children: [
            { path: '', pathMatch: 'prefix', redirectTo: 'home' },
            { path: 'home', component: HomeComponent }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(siteRoutes)],
    exports: [RouterModule]

})

export class SiteRoutingModule { }