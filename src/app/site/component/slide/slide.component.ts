import { Component, OnInit } from '@angular/core';

import { SlideService } from './slide.service';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {
  slideImgUrls: object[];
  constructor(
    private slideService: SlideService
  ) { 
    this.slideImgUrls = this.slideService.getslideImgUrls();
  }

  ngOnInit() {
  }

}
