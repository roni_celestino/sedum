import { Component, OnInit } from '@angular/core';

// SERVIÇOS
import { NavbarService } from './navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private navbarService: NavbarService
  ) { }

  ngOnInit() { }

}
