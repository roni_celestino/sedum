import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentacaoGeralComponent } from './documentacao-geral.component';

describe('DocumentacaoGeralComponent', () => {
  let component: DocumentacaoGeralComponent;
  let fixture: ComponentFixture<DocumentacaoGeralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentacaoGeralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentacaoGeralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
