import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretariaEscolarComponent } from './secretaria-escolar.component';

describe('SecretariaEscolarComponent', () => {
  let component: SecretariaEscolarComponent;
  let fixture: ComponentFixture<SecretariaEscolarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretariaEscolarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretariaEscolarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
