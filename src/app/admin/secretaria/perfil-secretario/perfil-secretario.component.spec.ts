import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilSecretarioComponent } from './perfil-secretario.component';

describe('PerfilSecretarioComponent', () => {
  let component: PerfilSecretarioComponent;
  let fixture: ComponentFixture<PerfilSecretarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilSecretarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilSecretarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
