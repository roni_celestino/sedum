import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VidaLegalComponent } from './vida-legal.component';

describe('VidaLegalComponent', () => {
  let component: VidaLegalComponent;
  let fixture: ComponentFixture<VidaLegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VidaLegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VidaLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
