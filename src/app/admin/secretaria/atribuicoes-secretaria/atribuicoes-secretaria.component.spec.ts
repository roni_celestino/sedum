import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtribuicoesSecretariaComponent } from './atribuicoes-secretaria.component';

describe('AtribuicoesSecretariaComponent', () => {
  let component: AtribuicoesSecretariaComponent;
  let fixture: ComponentFixture<AtribuicoesSecretariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtribuicoesSecretariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtribuicoesSecretariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
