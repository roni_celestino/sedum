import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaDiaSecretariaComponent } from './dia-dia-secretaria.component';

describe('DiaDiaSecretariaComponent', () => {
  let component: DiaDiaSecretariaComponent;
  let fixture: ComponentFixture<DiaDiaSecretariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaDiaSecretariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaDiaSecretariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
