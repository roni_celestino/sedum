import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoEscolarComponent } from './historico-escolar.component';

describe('HistoricoEscolarComponent', () => {
  let component: HistoricoEscolarComponent;
  let fixture: ComponentFixture<HistoricoEscolarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoEscolarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoEscolarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
