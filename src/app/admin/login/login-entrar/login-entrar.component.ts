import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { LoginService } from '../login.service';


@Component({
  selector: 'app-login-entrar',
  templateUrl: './login-entrar.component.html'
})

export class LoginEntrarComponent implements OnInit {
  constructor(private loginService: LoginService) { }

  ngOnInit() { }

  onSignin(form: NgForm) {
    this.loginService.signin(form.value.email, form.value.password)
      .subscribe(
      tokenData => console.log(tokenData),
      error => console.log(error)
      );
  }
}
