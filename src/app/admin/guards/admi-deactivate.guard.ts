import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { IFormCanDeactivate } from './iform-candeactivate';
import { AlunosFormComponent } from './../alunos/alunos-form/alunos-form.component';


@Injectable()
export class AdminDeactivateGuard implements CanDeactivate<IFormCanDeactivate> {
        canDeactivate(
            component: IFormCanDeactivate,
            route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot
        ): Observable<boolean>|Promise<boolean>|boolean {
            console.log('Guarda de Desativação')
            return component.alertaFormulario();
    }
}
