import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { LoginService } from './../login/login.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  // tslint:disable-next-line:typedef-whitespace
  ) : Observable<boolean> | boolean{
    if (this.loginService/*.usuarioEstaAutenticado()*/){
      return true;
    }
    this.router.navigate(['login']);
     return false;
  }
}
