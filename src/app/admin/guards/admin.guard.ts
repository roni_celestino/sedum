import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CanActivateChild, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()

export class AdminGuard implements CanActivateChild {
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('Guarda de Rotas Fulhas');
    return true;
  }

}
