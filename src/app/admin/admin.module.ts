import { ImageCropperComponent } from 'ng2-img-cropper';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
    MdButtonModule,
    MdCheckboxModule,
    MdToolbarModule,
    MdSidenavModule,
    MdInputModule,
    MdButtonToggleModule,
    MdIconModule,
    MdMenuModule,
    MdCardModule,
    MdRadioModule,
    MdSelectModule,
    MdAutocompleteModule
} from '@angular/material';

import { AdminRoutingModule } from './admin.routing.module';
import { AdminComponent } from './admin.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { SidenavComponent } from './component/sidenav/sidenav.component';
import { FooterComponent } from './component/footer/footer.component';
import { AlunosComponent } from './alunos/alunos.component';
import { ProfessoresComponent } from './professores/professores.component';
import { EscolasComponent } from './escolas/escolas.component';
import { SecretariaComponent } from './secretaria/secretaria.component';
import { CoordenacaoComponent } from './coordenacao/coordenacao.component';
import { DiarioComponent } from './diario/diario.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { ConselhosComponent } from './conselhos/conselhos.component';
import { GestaoComponent } from './gestao/gestao.component';
import { FinanciamentoComponent } from './financiamento/financiamento.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { HistoricoEscolarComponent } from './historico-escolar/historico-escolar.component';
import { ApresentacaoComponent } from './secretaria/apresentacao/apresentacao.component';
import { SecretariaEscolarComponent } from './secretaria/secretaria-escolar/secretaria-escolar.component';
import { AtribuicoesSecretariaComponent } from './secretaria/atribuicoes-secretaria/atribuicoes-secretaria.component';
import { PerfilSecretarioComponent } from './secretaria/perfil-secretario/perfil-secretario.component';
import { DiaDiaSecretariaComponent } from './secretaria/dia-dia-secretaria/dia-dia-secretaria.component';
import { VidaLegalComponent } from './secretaria/vida-legal/vida-legal.component';
import { DocumentacaoGeralComponent } from './secretaria/documentacao-geral/documentacao-geral.component';
import { AlunosFormComponent } from './alunos/alunos-form/alunos-form.component';
import { AlunosDetalhesComponent } from './alunos/alunos-detalhes/alunos-detalhes.component';

// SERVIÇOS
import { AlunosService } from './alunos/alunos.service';

// GUARDAS
import { AdminGuard } from './guards/admin.guard';
import { AdminDeactivateGuard } from './guards/admi-deactivate.guard';
import { AlunosListaComponent } from './alunos/alunos-lista/alunos-lista.component';
import { BreadcrumbComponent } from './component/breadcrumb/breadcrumb.component';
import { ImgUploadComponent } from './component/img-upload/img-upload.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        MdButtonModule,
        MdCheckboxModule,
        MdToolbarModule,
        MdSidenavModule,
        MdInputModule,
        MdButtonToggleModule,
        MdMenuModule,
        MdIconModule,
        MdRadioModule,
        MdSelectModule,
        MdAutocompleteModule,
        AdminRoutingModule
        ],
    declarations: [
        ImageCropperComponent,
        AdminComponent,
        NavbarComponent,
        SidenavComponent,
        FooterComponent,
        AlunosComponent,
        ProfessoresComponent,
        EscolasComponent,
        SecretariaComponent,
        CoordenacaoComponent,
        DiarioComponent,
        RelatorioComponent,
        ConselhosComponent,
        GestaoComponent,
        FinanciamentoComponent,
        FuncionariosComponent,
        HistoricoEscolarComponent,
        ApresentacaoComponent,
        SecretariaEscolarComponent,
        AtribuicoesSecretariaComponent,
        PerfilSecretarioComponent,
        DiaDiaSecretariaComponent,
        VidaLegalComponent,
        DocumentacaoGeralComponent,
        AlunosFormComponent,
        AlunosDetalhesComponent,
        AlunosListaComponent,
        BreadcrumbComponent,
        ImgUploadComponent
    ],
    exports: [
        AdminComponent,
        NavbarComponent,
        SidenavComponent,
        FooterComponent,
        AlunosComponent,
        ProfessoresComponent,
        EscolasComponent,
        SecretariaComponent,
        CoordenacaoComponent,
        DiarioComponent,
        RelatorioComponent,
        ConselhosComponent,
        GestaoComponent,
        FinanciamentoComponent,
        FuncionariosComponent,
        HistoricoEscolarComponent,
        ApresentacaoComponent,
        SecretariaEscolarComponent,
        AtribuicoesSecretariaComponent,
        PerfilSecretarioComponent,
        DiaDiaSecretariaComponent,
        VidaLegalComponent,
        DocumentacaoGeralComponent,
        AlunosFormComponent,
        AlunosDetalhesComponent,
        AlunosListaComponent,
    ],
    providers: [
        AlunosService,
        AdminGuard,
        AdminDeactivateGuard
    ]

})


export class AdminModule { }
