import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivateChild, CanDeactivate } from '@angular/router';

import { AdminComponent } from './admin.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { DiarioComponent } from './diario/diario.component';
import { FinanciamentoComponent } from './financiamento/financiamento.component';
import { CoordenacaoComponent } from './coordenacao/coordenacao.component';

// secretaria
import { SecretariaComponent } from './secretaria/secretaria.component';
import { DocumentacaoGeralComponent } from './secretaria/documentacao-geral/documentacao-geral.component';
import { DiaDiaSecretariaComponent } from './secretaria/dia-dia-secretaria/dia-dia-secretaria.component';
import { AtribuicoesSecretariaComponent } from './secretaria/atribuicoes-secretaria/atribuicoes-secretaria.component';
import { ApresentacaoComponent } from './secretaria/apresentacao/apresentacao.component';
import { PerfilSecretarioComponent } from './secretaria/perfil-secretario/perfil-secretario.component';
import { SecretariaEscolarComponent } from './secretaria/secretaria-escolar/secretaria-escolar.component';
import { VidaLegalComponent } from './secretaria/vida-legal/vida-legal.component';
// secretaria fim

import { ConselhosComponent } from './conselhos/conselhos.component';
import { ProfessoresComponent } from './professores/professores.component';
import { EscolasComponent } from './escolas/escolas.component';

// ALUNOS
import { AlunosComponent } from './alunos/alunos.component';
import { AlunosListaComponent } from './alunos/alunos-lista/alunos-lista.component';
import { AlunosFormComponent } from './alunos/alunos-form/alunos-form.component';
import { AlunosDetalhesComponent } from './alunos/alunos-detalhes/alunos-detalhes.component';
// ALUNOS FIM

import { GestaoComponent } from './gestao/gestao.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';


// GUARDAS
import { AdminGuard } from './guards/admin.guard';
import { AdminDeactivateGuard } from './guards/admi-deactivate.guard';

const adminRoutes: Routes = [
    {
        path: '', component: AdminComponent,
        // canActivateChild: [AdminGuard],
        children: [
            {
                path: 'alunos', component: AlunosComponent, children: [
                    {
                        path: 'novo', component: AlunosFormComponent// ,
// canDeactivate: [AdminDeactivateGuard]
                    },
                    { path: 'alunos-lista', component: AlunosListaComponent },
                    { path: 'alunos-detalhes/:matricula', component: AlunosDetalhesComponent },
                    {
                        path: 'alunos-lista/:matricula/editar', component: AlunosFormComponent// ,
                        // canDeactivate: [AdminDeactivateGuard]
                    }
                ]
            },
            { path: 'professores', component: ProfessoresComponent },
            { path: 'funcionarios', component: FuncionariosComponent },
            { path: 'conselhos', component: ConselhosComponent },
            {
                path: 'secretaria', component: SecretariaComponent, children: [
                    { path: 'apresentacao', component: ApresentacaoComponent },
                    { path: 'secretaria-escolar', component: SecretariaEscolarComponent },
                    { path: 'atribuicoes-secretaria', component: AtribuicoesSecretariaComponent },
                    { path: 'perfil-secretario', component: PerfilSecretarioComponent },
                    { path: 'dia-dia-secretaria', component: DiaDiaSecretariaComponent },
                    { path: 'documentacao-geral', component: DocumentacaoGeralComponent },
                    { path: 'vida-legal-estabelecimento-ensino', component: VidaLegalComponent }
                ]
            },
            { path: 'coordenacao', component: CoordenacaoComponent },
            { path: 'financiamento', component: FinanciamentoComponent },
            { path: 'diario', component: DiarioComponent },
            { path: 'relatorio', component: RelatorioComponent },
            { path: 'gestao', component: GestaoComponent },

        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]

})

export class AdminRoutingModule { }
