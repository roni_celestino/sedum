import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder, NgModel, FormControlName } from '@angular/forms';
import { AlunosService } from 'app/admin/alunos/alunos.service';

@Component({
  selector: 'app-alunos-form',
  templateUrl: './alunos-form.component.html',
})

export class AlunosFormComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private alunosService: AlunosService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nome: [null],
      data_hora_cadastro: [null],
      mat_final: [null],
      foto: [null],
      mat_educacenso: [null],
      ident_inep: [null],
      data_nasc: [null],
      naturalidade: [null],
      uf: [null],
      rg: [null],
      org: [null],
      uf_rg: [null],
      data_exp: [null],
      nacionalidade: [null],
      doc_estrangeiro: [null],
      tel_fixo: [null],
      celular: [null],
      vida_escolar: [null],
      dificuldade_fala: [null],
      idade_certa: [null],
      deficiencia: [null],
      trans_global: [null],
      altas_hab: [null],
      difi_locomacao: [null],
      desempenho_ed: [null],
      tipo_certidao: [null],
      modelo_cert: [null],
      ant_n_termo: [null],
      ant_folha: [null],
      ant_livro: [null],
      ant_data_emissao: [null],
      ant_nome_cartorio: [null],
      ant_mun_cart: [null],
      ant_uf_cart: [null],
      nov_n_mat: [null]



    });
  }

  onSubmit(form: NgForm) {
    this.alunosService.addAluno(form.value)
      .subscribe(
      () => alert('Quote created!')
      );
    form.reset();
  }


}


