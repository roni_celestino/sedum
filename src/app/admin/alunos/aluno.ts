export class Aluno {
  nome: string;
  matricula: string;
  matricula_aluno: string;
  matricula_final: string;
  mat_educacenso: string;
  ident_inep: string;
  nis: string;
  data_nasc: string;
  naturalidade: string;
  uf: string;
  rg: number;
  org: string;
  uf_rg: string;
  data_exp: string;
  sexo: string;
  raca: string;
  nacionalidade: string;
  doc_estrangeiro: string;
  tel_fixo: string;
  celular: string;
  vida_escolar: string;
  dificuldade_fala: string;
  idade_certa: string;
  deficiencia: string;
  trans_global: string;
  altas_hab: string;
  difi_locomocao: string;
  desempenho_ed: string;
  tipo_certidao: string;
  modelo_cert: string;
  ant_n_termo: string;
  ant_folha: string;
  ant_livro: string;
  ant_data_emissao: string;
  ant_nome_cartorio: string;
  ant_mun_cart: string;
  ant_uf_cart: string;
  nov_n_mat: string;
  mat_final: string;
  titulo: string;
  conteudo: string;

  constructor() {
  }
}
