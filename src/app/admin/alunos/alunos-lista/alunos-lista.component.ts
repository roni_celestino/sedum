import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { Aluno } from '../aluno';
import { AlunosService } from '../alunos.service';
import { Http } from "@angular/http";


@Component({
  selector: 'app-alunos-lista',
  templateUrl: './alunos-lista.component.html'
})

export class AlunosListaComponent implements OnInit {
  alunos: Aluno[];
  errorMessage: String;

  constructor(
    private alunosService: AlunosService,
    private router: Router,
    http: Http) { }

  ngOnInit(): void {
    this.todosAluno();
  }

  todosAluno() {
    this.alunosService.getAllAluno()
      .subscribe(
        alunos => this.alunos = alunos,
        (error: Response) => console.log(error)
      );
  }

}


