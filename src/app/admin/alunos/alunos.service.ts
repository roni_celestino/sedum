import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';


import { Aluno } from './aluno';
import { LoginService } from '../login/login.service';

@Injectable()
export class AlunosService {

  private url = 'http://localhost/api/';

  constructor(
    private http: Http,
    private loginService: LoginService
  ) { }

  getAllAluno(): Observable<Aluno[]> {
    return this.http
      .get(this.url + 'alunos')
      .map(
      (response: Response) => {
        return response.json().alunos;
      });
  }

  addAluno(form) {
    const token = this.loginService.getToken();
    const body = JSON.stringify(form.value);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('http://localhost/api/quote?token=' + token, body, { headers: headers })
  }

}
















